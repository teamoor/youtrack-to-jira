#!/bin/sh
set -e
PROJECT_KEY=${1:?}

http \
	--verify=no \
	--auth="$JIRA_AUTH" \
	DELETE \
    "$JIRA_URL/rest/api/2/project/${PROJECT_KEY}" \
    "Accept:application/json" 
