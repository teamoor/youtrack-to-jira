#!/bin/sh
YT_PROJECT_KEY=${1:?}
PROJECT_KEY=${2:?}
FIELD=${3:?}

./get-bundle-values.sh $YT_PROJECT_KEY "$FIELD" \
| jq -c --arg PROJECT_KEY $PROJECT_KEY '
	.[]
	| { 
		name, released, archived,
	 	releaseDate: (
	 	 	if .releaseDate then
	 	 		.releaseDate / 1000 | strftime("%Y-%m-%d")
	 	 	else
	 	 		null
	 	 	end),
	 	project: $PROJECT_KEY
	}' \
| while read line ; do
	echo "$line" | ./jira-post.sh version
done
