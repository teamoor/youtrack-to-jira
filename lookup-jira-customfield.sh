#!/bin/sh
NAME=${1:?}
./jira-get.sh customFields maxResults==9999 \
| jq --arg name "$NAME" '
	.values[] | select(.name == $name)
' 