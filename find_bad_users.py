import json
import sys


for username, results in json.load(sys.stdin).items():
	if not results:
		print(username)
		continue

	exact = list(
		item
		for item in results
		if item['name'].lower() == username.lower()
	)
	if not exact:
		print(username)
		continue

