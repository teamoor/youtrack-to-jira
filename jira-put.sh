#!/bin/sh
set -e
URI_PATH=${1:?}
shift

http \
    "--auth=$JIRA_AUTH" \
    --verify=no \
	--session=".jira-session.json" \
    PUT \
    "$JIRA_URL/rest/api/2/$URI_PATH" \
    "Accept:application/json" \
    "Content-Type:application/json"
