#!/bin/sh

JIRA_PROJECT_KEY=${1:?}
while read line ; do
    http \
    	--body \
    	--ignore-stdin \
        --auth="$JIRA_AUTH" \
        --verify=no \
        GET "$JIRA_URL/rest/api/2/user/assignable/search" \
        "project==$JIRA_PROJECT_KEY" \
        "username==$line" \
    | jq  --arg username "$line" '{($username): .}'
done