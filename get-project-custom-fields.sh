#!/bin/sh

PROJECT_KEY=${1:?}
FIELDS='
customFields(
    field(
        id,
        name,
        localizedName,
        fieldType(
           id
        ),
        isAutoAttached,
        isDisplayedInIssueList,
        ordinal,
        aliases,
        fieldDefaults(
            emptyFieldText,
            isPublic,
            canBeEmpty
        ),
        hasRunningJob,
        isUpdateable
    ),
    bundle(
        id,
        isUpdateable,
        name,
        values(
            archived,
            releaseDate,
            released,
            id,
            name
        )
    )
)'
FIELDS=$(
    # Strip all whitespace including newlines 
    echo "$FIELDS" | ./strip-and-mash-lines.sh 
)
http --verify=no GET \
    "$YT_URL/api/admin/projects/$PROJECT_KEY" \
    "fields==$FIELDS" \
    Accept:application/json \
    "Authorization:Bearer $YT_TOKEN" \
    Content-Type:application/json