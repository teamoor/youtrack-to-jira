#!/bin/sh
# Strip all whitespace including newlines

sed -e ':a' -e 'N;$!ba' -e 's/\n//g' -e 's/[[:space:]]//g'