import atexit
import os
import shlex
import sys
import json
import requests
import subprocess
import urllib3
from argparse import Namespace
from copy import deepcopy
from datetime import datetime
from functools import reduce, partial
from operator import getitem
from pathlib import Path

import click
from eliot.json import EliotJSONEncoder
from eliot import (
    log_call,
    start_action,
    start_task,
    to_file,
    write_traceback,
    add_destinations,
    log_message
)


class JSONEncoder(EliotJSONEncoder):
    def default(self, obj):
        if callable(obj):
            name = getattr(obj, '__name__', None)
            if name:
                return f'{name}()'
        if isinstance(obj, (Path, bytes)):
            return str(obj)
        if isinstance(obj, State):
            obj = { k: v
                for k, v in vars(State).items()
                if k not in ['jissues']
            }
        return EliotJSONEncoder.default(self, obj)


urllib3.disable_warnings()


jurl = (lambda path: (
    f'{os.environ["JIRA_URL"]}'
    f'{path}')
)

first = lambda xs: next(iter(xs), None)

def http_session():
    s = requests.Session()
    s.auth = tuple(
        os
        .environ['JIRA_AUTH']
        .split(':', 1)
    )
    s.verify = False
    # cookies_file = Path('jira-cookies.json')
    # if cookies_file.exists():
    #     s.cookies.update(
    #         json.load(
    #            cookies_file.open('r') 
    #         )
    #     )

    # @atexit.register
    # def logout():
    #     s.delete(
    #        jurl('/rest/auth/1/session') 
    #     )
    # @atexit.register
    # def dump_session():
    #     json.dump(
    #         dict(s.cookies),
    #         cookies_file.open('w') 
    #     )
 
    return s


def get_json(http, path, params=None):
    with start_action(
        action_type='http-get',
        path=path,
        params=params,
    ) as action:
        resp = http.get(jurl(path), params=params) 
        try:
            content = resp.json()
        except json.JSONDecodeError:
            content = resp.content
        action.log(
            message_type='response',
            status=resp.status_code,
            content=content,
        )
        resp.raise_for_status()
    return content


def post_json(http, path, data):
    with start_action(
        action_type='http-post',
        path=path,
        data=data
    ) as action:
        resp = http.post(
            jurl(path),
            json=data,
        )
        try:
            content = resp.json()
        except json.JSONDecodeError:
            content = resp.content
        action.log(
            message_type='response',
            status=resp.status_code,
            content=content,
        )
        resp.raise_for_status()
    return content 


def post_file(http, path, files):
    with start_action(
        action_type='http-post-file',
        path=path,
    ) as action:
        resp = http.post(
            jurl(path),
            files=files,
            headers={
                'X-Atlassian-Token': 'no-check'
            }
        )
        action.log(
            message_type='response',
            status=resp.status_code,
            content=resp.text, 
        )
        resp.raise_for_status()
        data = resp.json()
    return data 


class indexed(list):
    '''
    A list that also keeps an index mapping:
        key(item) -> item index

    Can be indexed with keys in addition
    to regular int indices
    '''

    def __init__(self, it=tuple(), key=id):
        self.key = key
        self.index = {
            key(item): i
            for i, item in enumerate(it)
        }
        super().__init__(it)

    def __getitem__(self, arg):
        if isinstance(arg, str):
            return self[self.index[arg]]
        return super().__getitem__(arg)

    def __setitem__(self, arg, value):
        super().__setitem__(arg, value)

        if not isinstance(arg, slice):
            self.index[self.key(value)] = arg
            return

        # Loop over slice items
        # w/ their respective indices 
        slice_ = arg
        step = slice_.step or 0
        self.index.update(
            (
                self.key(item),
                i + step,
            )
            for i, item in enumerate(
                self[slice_],
                slice_.start,
            )
        )


def expand_linked_yissues(indexed_yissues):
    key = indexed_yissues.key
    def map_yissues(arg):
        arg['issues'] = [
            indexed_yissues[key(yissue)]
            for yissue in arg['issues']
        ]
    for yissue in indexed_yissues:
        try:
            #map_yissues(yissue['parent'])
            #map_yissues(yissue['subtasks'])
            for link in yissue['links']:
                map_yissues(link)
        except KeyError as e:
            log_message(
                message_type='missing-linked',
                this_side=key(yissue),
                other_side=e.args[0],
            )


def ensure_state(ctx):
    if not ctx.obj:
        ctx.obj = State(**(ctx.params or {}))
        ctx.ensure_object(State)
    return ctx.obj



class State(Namespace):
    def __init__(self, **kwargs):
        self.reached_start_ykey = None
        self.ykey_to_jkey = dict()
        self.jissues = dict()
        self.seen = set()
        self.action = None
        vars(self).update(
            **kwargs,
        )


@click.command()
@click.argument('project_key')
@click.option(
    '--start-at-ykey'
)
@click.option(
    '--bulk',
    is_flag=True,
)
@click.option(
    '--label',
)
@click.option(
    '--chunk-size',
    type=int,
    default=5,
)
@click.option(
    '--start-at',
    type=int,
)
@click.option(
    '--limit',
    type=int,
)
@click.option(
    '--fetch-files',
    is_flag=True,
)
@click.option(
    '--check-files',
    is_flag=True,
)
@click.option(
    '--skip-size-matching/--no-skip-size-matching',
    default=True,
)
@click.option(
    '--post-files',
    is_flag=True,
)
@click.option(
    '--links',
    is_flag=True,
)
@click.option(
    '--create',
    is_flag=True,
)
@click.option(
    '--attach-json',
    is_flag=True,
)
@click.option(
    '--groovy-script',
)
@click.option(
    '--map/--no-map',
    help='Find matching Jira issues',
    default=True,
)
@click.option(
    '--extra-jql',
)
@click.option(
    '--progress/--no-progress',
    default=True,
)
@click.option(
    '--skip-missing-linked/--no-skip-missing-linked',
    default=True,
)
@click.option(
    '--meta-file',
    type=click.File('r')
)
@click.option(
    '--json-log-file',
    type=click.File('a'),
)
@click.option(
    '--attachments-dir',
    type=click.Path(),
    default='output',
)
@click.pass_context
def main(
    ctx,
    project_key,
    start_at_ykey,
    bulk,
    label,
    chunk_size,
    start_at,
    limit,
    fetch_files,
    check_files,
    skip_size_matching,
    post_files,
    links,
    create,
    attach_json,
    groovy_script,
    map,
    extra_jql,
    progress,
    skip_missing_linked,
    meta_file,
    json_log_file,
    attachments_dir,
):
    action = first(
        param
        for param in (
            'fetch-files check-files bulk '
            'attach-json post-files links '
            'groovy-script'
        ).split()
        if ctx.params[
            param.replace('-', '_')
        ]
    )
    if not json_log_file:
        path = Path('output', project_key, 'log')
        path.mkdir(parents=True, exist_ok=True)
        json_log_file = (
            path
            .joinpath(f'{action}.json')
            .open('a')
        )

    @atexit.register
    def tail_log_to_eliot_tree():
        if not sys.stdout.isatty():
            return
        if not any(sys.exc_info()):
            return
        os.system(
            'tail -5 '
            f'{json_log_file.name}'
            ' | eliot-tree'
        )

    to_file(json_log_file, encoder=JSONEncoder)

    state = ensure_state(ctx)
    state.action = action
    state.reached_start_ykey = None
    state.http = http = http_session()
    state.key = key = lambda x: x['idReadable']
    bar_template = (
        '[%(bar)s] %(info)-18s %(label)18s'
    )

    yissues = indexed(
        json.load(sys.stdin),
        key=key,
    )
    if check_files:
        selected = list(yissue_selector(
                state,
                yissues
        ))
        with click.progressbar(
            selected,
            label=action
        ) as bar:
            bar.is_hidden = not state.progress
            collected = [
                list(pending_download_items(
                    state,
                    yissue,
                ))
                for yissue in bar
            ]

        for items in collected:
            for item in items:
                json.dump(
                    item,
                    sys.stdout,
                )
                print()
        return

    if fetch_files:
        selected = list(yissue_selector(
            state,
            yissues,
        ))
        pending = list(
            with_pending_downloads(
                state,
                selected,
            )
        )
        total_size = sum( 
            sum([i['size'] for i in items])
            for yissue, items in pending
        )
        with click.progressbar(
            label=action,
            length=total_size,
            bar_template=bar_template,
            show_pos=True,
        ) as bar:
            bar.is_hidden = not state.progress
            for yissue, items in pending:
                download_attached_files(
                    state,
                    yissue,
                    items,
                )
                bar.update(
                    sum(item['size'] for item in items)
                )
        return


    if not attach_json:
        expand_linked_yissues(yissues)

    state.yissues = yissues

    if not meta_file:
        state.project = fetch_project(
            http,
            project_key,
        )
        state.linktypes = get_json(
            http,
            f'/rest/api/2/issueLinkType',
        )['issueLinkTypes']

        state.customfields = list(
            pager(
                http,
                f'/rest/api/2/customFields',
            )
        )
    else:
        meta = json.load(meta_file)
        state.project = meta['project']
        state.linktypes = meta['linktypes']
        state.customfields = meta['customfields']


    if map:
        map_issues(
            state,
            os.environ['JIRA_YT_KEY_FIELD'],
        )

    if post_files:
        selected = list(yissue_selector(
            state,
            yissues,
        ))
        pending = list(
            with_pending_uploads(
                state,
                selected,
            )
        )
        total_size = sum( 
            sum([i['size'] for i in items])
            for yissue, items in pending
        )

        with click.progressbar(
            label=action,
            length=total_size,
            bar_template=bar_template,
        ) as bar:
            bar.is_hidden = not state.progress
            for yissue, items in pending:
                upload_attached_files(
                    state,
                    yissue,
                    items,
                )
                bar.update(
                    sum(item['size'] for item in items)
                )

        return        

    if bulk:
        yissues = limit_yissues(state, yissues)

        chunks = [
            yissues[i:i + chunk_size]
            for i in range(0, len(yissues), chunk_size)
        ]
        with click.progressbar(
            length=len(yissues),
            label=action,
            bar_template=bar_template,
        ) as bar:
            bar.is_hidden = not state.progress
            for chunk in chunks:
                # output = bulk_convert(state, chunk),
                updates = (
                    jissue
                    for ykey, jissue in
                    jissue_generator(state, chunk)
                )
                body = dict(
                    issueUpdates=list(updates)
                )
                if not create:
                    json.dump(
                        body,
                        sys.stdout,
                        separators=(',', ':'),
                    )
                    print()
                    continue

                with start_action(
                    action_type='bulk-create',
                    ykeys=[ state.key(yissue) for yissue in chunk ],
                ) as action:
                    response = bluk_create_jissues(state, body)
                    for error in response.get('errors', []):
                        n = error.get('failedElementNumber')
                        yissue = chunk[n] if n is not None else None 
                        fields = {
                            k: (yissue or {}).get(k)
                            for k in error.get('errors', {})
                        }
                        action.log(
                            message_type='error',
                            error=error,
                            yissue=yissue,
                            fields=fields,
                        )
                bar.update(chunk_size)
        return

    if attach_json:
        selected = yissue_selector(
            state,
            yissues
        )
        pending = list(
            with_pending_json_uploads(
               state,
               selected
            )
        )
        with click.progressbar(
            pending,
            label=state.action,
            bar_template=bar_template,
       ) as bar:
            bar.is_hidden = not state.progress
            for yissue in bar:
                attach_yissue_json(state, yissue)
        return

    if groovy_script and state.label:
        tmpl = open(groovy_script).read()
        extra_jql = (
            f' AND labels in ("{state.label}")'
        )
        script_text = tmpl % dict(
            project_key=state.project_key,
            extra_jql=extra_jql,
            username=os.environ['JIRA_DEFAULT_USERNAME'],
            key_field=os.environ['JIRA_YT_KEY_FIELD'],
            status_field=os.environ['JIRA_YT_STATUS_FIELD'],
        ) 
        target_file = '.'.join([
            groovy_script.split('.')[0],
            state.label,
            'groovy',
        ])
        open(target_file, 'w').write(script_text)
        # url = jurl('/rest/scriptrunner/latest/user/exec/')
        # curl_args = [
        #     'curl',
        #     '--show-error',
        #     '--insecure',
        #     '--user',
        #         os.environ['JIRA_AUTH'],
        #     '--request',
        #         'POST',
        #     url,
        #     '--header',
        #         'X-Atlassian-token: no-check',
        #     '--header',
        #         'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
        #     '--header', 
        #         'Accept: application/json',
        #     '--data-urlencode',
        #         f'scriptText@{target_file}',
        #     url
        # ]
        # curl = subprocess.run(
        #     curl_args,
        #     capture_output=True,
        #     check=True,
        # )
        # print(str(curl.stdout))

        # path = '/rest/scriptrunner/latest/user/exec/'
        # data = dict(
        #     script=script_text,
        #     scriptPath=None,
        #     parameters={},
        # )
        # with start_action(
        #     action_type='http-post',
        #     path=path,
        #     data=data
        # ) as action:
        #     resp = http.post(
        #         jurl(path),
        #         data=dict(
        #             scriptText=script_text
        #         ),
        #         headers={
        #             'X-Atlassian-token': 'no-check'
        #         },
        #     )
        #     try:
        #         content = resp.json()
        #     except json.JSONDecodeError:
        #         content = resp.content
        #     action.log(
        #         message_type='response',
        #         status=resp.status_code,
        #         content=content,
        #     )
        #     resp.raise_for_status()
        # json.dump(
        #     content,
        #     sys.stdout,
        # )
        # print()
        return


    if links:
        jlinks = list(
            jissue_link_generator(
                state,
                yissues,
            )
        )
        with click.progressbar(
            jlinks,
            label=action,
            bar_template=bar_template,
        ) as bar:
            bar.is_hidden = not state.progress
            for jlink in bar:
                if not create:
                    # json.dump(
                    #     jlink,
                    #     sys.stdout,
                    #     separators=(',', ':')
                    # )
                    # print()
                    continue

                with start_action(
                    action_type='create-jlink',
                    jlink=jlink,
                ):
                    create_jlink(state, jlink)

            return

    if map:
        json.dump(
            state.ykey_to_jkey,
            sys.stdout,
        )
        print()
        return

    # if create:
    #     import_lines(state)
    #     return 

    # for ykey, jissue in jissue_generator(
    #     state,
    #     yissues,
    # ):
    #     json.dump(
    #         jissue,
    #         sys.stdout,
    #         separators=(',', ':')
    #     )
    #     print()


def yissue_selector(state, yissues):
    count = 0
    for yissue in yissues:
        if state.limit and count >= state.limit:
            break

        if state.start_at_ykey and not state.reached_start_ykey:
            if state.key(yissue) != state.start_at_ykey:
                continue
            else:
                state.reached_start_ykey = True

        yield yissue
        count +=1


def limit_yissues(state, yissues):
    if state.map:
        key_map = state.ykey_to_jkey
        yissues = [
            yissue
            for yissue in yissues
            if state.key(yissue) not in key_map
        ]

    if state.start_at:
        yissues = yissues[state.start_at:]

    if state.limit:
        yissues = yissues[:state.limit]

    return yissues


def fetch_project(http, project_key):
    base = b = '/rest/api/2'
    project = get_json(
        http,
        f'{b}/project/{project_key}'
    )
    project['issuetypes'] = (
        get_json(
            http, (
                f'{b}/issue/createmeta'
                f'/{project_key}/issuetypes'
            )
        )
    )
    project['priorityscheme'] = ps = get_json(
        http,
        f'{b}/project/{project_key}/priorityscheme'
    )
    project['priorities'] = [
        priority
        for priority in get_json(
            http,
            f'{b}/priority'
        )
        if priority['id'] in ps['optionIds']
    ]

    return project


def pager(
    http,
    uri,
    size=1000,
    params=None,
    items_key='values',
    **kw
):
    count = 0
    params = dict(params) if params else dict()
    params['maxResults'] = size
    while True:
        params['startAt'] = count
        result = get_json(
            http,
            uri, 
            params=params,
        )
        values = result[items_key]

        for value in values: 
            yield value
            count += 1

        if result.get('isLast'):
            break

        if count >= result['total']:
            break



def jissue_link_generator(state, yissues):
    '''
    '''
    key = state.key
    seen = state.seen
    for yissue in yissues:
        ykey = key(yissue)
        if (
            state.map
            and
            ykey not in state.ykey_to_jkey
        ):
            continue

        if ykey in seen:
            log_message(
                message_type='skip-seen',
                ykey=ykey
            )
            continue

        for link in yissue['links']:
            link_type_name = link['linkType']['name']
            link_direction = link['direction']

            # Subtasks are handled separately
            # if link_type_name == 'Subtask':
            #     continue

            # Internaly Jira issue link is:
            #   (source, destination, type())
            # Therefore consider only one side
            # of the link or non-directional links
            if link_direction not in ['OUTWARD', 'BOTH']:
                continue

            for other_yissue in link['issues']:
                dst_ykey = key(other_yissue)

                if dst_ykey in seen:
                    log_message(
                        message_type='skip-seen-other',
                        ykey=dst_ykey
                    )
                    continue

                try:
                    jlink = convert_ylink(
                        state,
                        ykey,
                        dst_ykey,
                        link,
                    )
                except IssueKeyMappingError as e:
                    if state.skip_missing_linked:
                        continue
                    raise

                matching = [
                    item
                    for item in yissue_jlinks(
                        state,
                        yissue
                    ) 
                    if (
                        item['type']['name'] == 
                        jlink['type']['name']
                    ) and (
                        item['inwardIssue']['key'] ==
                        jlink['inwardIssue']['key']
                    ) and (
                        item['outwardIssue']['key'] ==
                        jlink['outwardIssue']['key']
                    )
                ]
                jkey = map_ykey(state, ykey)
                if any(matching):
                    log_message(
                        message_type='skip-existing-link',
                        ykey=ykey,
                        jkey=jkey,
                        existing=matching,
                    )
                    continue

                yield jlink


@log_call(include_args=[])
def yissue_jlinks(state, yissue):
    ykey = state.key(yissue)
    jkey = state.ykey_to_jkey[ykey]
    jissue = state.jissues[jkey]
    return (
        jissue['fields']['issuelinks']
    )


def jissue_generator(state, yissues):
    key = state.key
    seen = state.seen
    count = 0
    for yissue in yissue_selector(state, yissues):
        ykey = key(yissue)
        if state.map and ykey in state.ykey_to_jkey:
            log_message(
                message_type='skip-imported',
                ykey=ykey
            )
            continue
        if ykey in seen:
            log_message(
                message_type='skip-seen',
                ykey=ykey
            )
            continue
        try:
            try:
                yield ykey, convert_yissue(
                        state,
                        yissue,
                )
            except SkipIssue:
                continue
            count += 1
        except ConverterError as e:
            log_message(
                message_type='converter',
                args=e.args,
            )


# def bulk_convert(state, yissues):
#     return dict(
#         issueUpdates=[
#             jissue
#             for ykey, jissue in
#             jissue_generator(state, yissues)
#         ]
#     )


def convert_yissue(state, yissue):
    ykey = state.key(yissue)
    state.seen.update(ykey)
    project = state.project

    standard_fields = [
        'reporter',
        'assignee',
        'summary',
        'issuetype',
        'priority',
    ]
    converters = {
        field: globals()[field] 
        for field in standard_fields
    }
    fields = dict(
        project=dict(
            key=project['key']
        )
    )
    with start_task(
        task_type='convert',
        issue_id=yissue['idReadable']
    ) as action:
        try:
            fields.update(**{
                field: func(state, yissue)
                for field, func in (
                    converters
                    .items()
                )
            })
            
            # p = parent(state, yissue)
            # if p:
            #     parent_ykey = state.key(p)
            #     parent_jkey = state.ykey_to_jkey[parent_ykey]
            #     fields.update(
            #         parent=dict(
            #             key=parent_jkey
            #         )
            #     )

            description_ = description(
                state,
                yissue,
            )
            if description_:
                fields.update(
                    description=description_
                )

            for producer in JFIELD_PRODUCERS:
                result = None
                with start_action(
                    action_type='producer'
                ) as action:
                    result = producer(state, yissue)
                    action.log(
                        message_type='result',
                        result=result
                    )
                if result is None:
                    continue
                fields.update({result[0]: result[1]})

        except:
            action.log(
                message_type='bad-yissue',
                key=ykey,
            )
            raise

    if state.label:
        fields.setdefault(
            'labels',
            []
        ).append(
            state.label
        )

    return dict(fields=fields)


def map_issues(state, key_field):
    project_key = state.project['key']
    numeric_id = lookup_jcustom_field(
        state,
        key_field
    )['numericId']
    field_id = f'customfield_{numeric_id}'

    jql=(
        f'project = {project_key}'
        f' AND'
        f' "{key_field}" IS NOT EMPTY'
    )
    if state.extra_jql:
        jql += f' AND {state.extra_jql}'
    if state.label:
        jql += f' AND labels in ("{state.label}")'

    fields = [
        'key',
        field_id,
        'issuelinks',
        'attachment'
    ]

    def ensure_both_ends(jissue):
        for link in jissue['fields']['issuelinks']:
            for key in  ['outwardIssue', 'inwardIssue']:
                if key in link:
                    continue
                link[key] = dict(
                    key=jissue['key'],
                    id=jissue['id'],
                )
        return jissue

    jissues = [
        ensure_both_ends(jissue)
        for jissue in pager(
            state.http,
            '/rest/api/2/search',
            size=1000,
            params=dict(
                jql=jql,
                fields=fields
            ),
            items_key='issues',
        )
    ]
    state.jissues = {
        jissue['key']: jissue 
        for jissue in jissues
    }

    state.ykey_to_jkey = {
        jissue['fields'][field_id]: jissue['key']
        for jissue in state.jissues.values()
    }
    return state.ykey_to_jkey


# XXX Use in jcustom_field
def lookup_jcustom_field(state, name):
    return [
        field
        for field in state.customfields
        if field['name'] == name
    ][0]


def reporter(state, yissue):
    return dict(
        name=map_field_value(
            'reporter',
             yissue['reporter']['login'],
        )
    )


class ConverterError(ValueError):
    def __init__(self, field, value):
        self.args = (field, value)


def summary(state, yissue):
    value = yissue['summary']
    max_length = 255
    # if (value and len(value) > max_length):
    #     raise ConverterError('summary', value)
    if value is None:
        return

    if len(value) > max_length:
        return value[:max_length - 1] + '✄' 
    
    return value


def description(state, yissue):
    value = yissue['description']
    max_length = 32767
    # if (value and len(value) > max_length):
    #     raise ConverterError('description', value)
    if value is None:
        return

    if len(value) > max_length:
        return value[:max_length - 1] + '✄' 

    return value


class UnknownLinkType(ValueError):
    pass


class IssueKeyMappingError(LookupError):
    pass


def map_ykey(state, ykey):
    try:
        return state.ykey_to_jkey[ykey]
    except KeyError as e:
        raise IssueKeyMappingError(*e.args)


def convert_ylink(state, src_ykey, dst_ykey, link):
    with start_action(
        action_type='convert_ylink',
        src_ykey=src_ykey,
        dst_ykey=dst_ykey,
        type=link['linkType'],
    ) as action:
        if state.map:
            try:
                src_jkey = map_ykey(state, src_ykey) 
                dst_jkey = map_ykey(state, dst_ykey)
            except KeyError as e:
                raise IssueKeyMappingError(*e.args)

        linktype = map_field_value(
            'linktype',
            link['linkType']['name'],
        )
        if isinstance(linktype, str):
            name = linktype
            reverse = False
        else:
            name = linktype['name']
            reverse = linktype.get('reversed', False)

        if name not in [
            type_['name'] for type_ in state.linktypes
        ]:
            raise UnknownLinkType(name)

        outward_key = (
            src_jkey
            if not reverse 
            else dst_jkey
        )
        inward_key = (
            dst_jkey
            if not reverse 
            else src_jkey
        )
        jlink = dict(
            type=dict(
                name=name
            ),
            outwardIssue=dict(
                key=outward_key
            ),
            inwardIssue=dict(
                key=inward_key
            ),
        )
        action.log(
            message_type='jlink',
            jlink=jlink,
        )
    return jlink


def get_ycustom_field(yissue, field_name):
    return [
        i for i in yissue['customFields']
        if i['name'] == field_name
    ][0]


def map_field_value(field_name, value):
    field_mapping = MAPPING[field_name]
    if callable(field_mapping):
        return field_mapping(value)
    return field_mapping.get(value, value)


def user_mapper(blacklist, default):
    def mapper(value):
        if value in blacklist:
            return default
        return value
    return mapper


def assignee(state, yissue):
    field = get_ycustom_field(
        yissue,
        'Assignee',
    )
    if not field:
        return
    value = field.get('value')
    if not value:
        return
    name = map_field_value(
        'assignee',
        value['login'],
    )
    return dict(name=name)


class SkipIssue(Exception):
    pass


# For use in MAPPING.issuetype
IGNORE = object()


def issuetype(state, yissue):
    yname = get_ycustom_field(
        yissue,
        'Type',
    )['value']['name']

    # mapper = (
    #     'subtask_issuetype'
    #     if parent(state, yissue)
    #     else 'issuetype'
    # )
    default = (
        MAPPING
        ['issuetype']
        .get(None, yname)
    )
    jname = (
        MAPPING
        ['issuetype']
        .get(yname, default)
    )

    if jname is IGNORE:
        raise SkipIssue()

    issuetypes = (
        state
        .project
        ['issuetypes']
        ['values']
    )
    found = [
        issue_type 
        for issue_type in issuetypes
        if issue_type['name'] == jname
    ]
    if found:
        return dict(
            id=found[0]['id']
        )

    raise ValueError('issuetype', yname)


def priority(state, yissue):
    name = get_ycustom_field(
        yissue,
        'Priority'
    )['value']['name']
    name = MAPPING['priority'].get(name, name) 
    project = state.project
    found = [
        priority
        for priority in project['priorities']
        if priority['name'] == name
    ]
    if found:
        return dict(id=found[0]['id'])
    raise ConverterError('priority', name)


def parent(state, yissue):
    parent_yissues = yissue['parent']['issues']
    assert len(parent_yissues) <= 1
    try:
        found = [
            state.yissues[state.key(i)]
            for i in parent_yissues 
        ]
    except KeyError:
        return
    return found and found[0] or None




def copy(source, target, converter=None):
    missing = object()

    def copier(state, yissue):
        if not isinstance(source, (list, tuple)):
            srcs = [source]
        else:
            srcs = source

        value = missing
        failed = []
        for src in srcs:
            getter = (
                src
                if callable(src)
                else
                lambda state, yissue: yissue[src]
            )
            with start_action(
                action_type='getter',
                getter=getter,
                src=src,
            ):
                try:
                    value = getter(state, yissue) 
                except LookupError as e: 
                    log_message(
                        message_type='getter-error',
                        getter=getter,
                        src=src,
                        arg=e.args[0],
                    )
                    failed.append(e.args[0])
                    continue

        if value is missing:
            raise LookupError(failed)

        if callable(target):
            dst = target(state, yissue)
        else:
            dst = target

        if converter:
            value = converter(state, yissue, value)
        return (dst, value)

    return copier



def dump_json(path, dst):
    parts = [
        int(part)
        if part.startswith(
            tuple('0123456789')
        )
        else part
        for part in path.split('.')
        if part
    ]
    getter = partial(reduce, getitem, parts)

    def dumper(state, yissue):
        key = (
            dst(state, yissue)
            if callable(dst)
            else dst
        )
        return key, json.dumps(
            getter(yissue)
        )
        
    return dumper


def millis_to_jira(state, yissue, value):
    # Milliseconv
    if value is None:
        return
    value = int(value / 1000)
    return (
        datetime
        .fromtimestamp(value)
        .astimezone()
        .replace(microsecond=0)
        .strftime('%Y-%m-%dT%H:%M:%S.000%z')
    )


def dump_comments(state, yissue, value):
    data = json.dumps(value)
    return data


class IncorrectFieldSpec(Exception):
    pass


def ycustom_field(name, property='name'):
    def getter(state, yissue):
        field = get_ycustom_field(yissue, name)
        value = field.get('value')
        if value is None:
            return
        return value.get(property)

    return getter


def jcustom_field(name=None, id=None):
    if not any([name, id]):
        raise TypeError(
            'Provide at least Jira custom field name or ID'
        )
    if id is not None and not isinstance(id, int):
        raise IncorrectFieldSpec(
            'Please use numeric Jira custom field ID'
        )

    def wrapped(state, yissue):
        if name is not None:
            by_name = [
                field
                for field in state.customfields
                if field['name'] == name
            ]
            if (
                len(by_name) > 1
                and
                id is None
            ):
                raise IncorrectFieldSpec(
                    f'Found multiple Jira '
                    f'custom fields named "{name}"'
                    f': {by_name}'
                )
            if len(by_name) == 1:
                found = by_name[0]
                if (
                    id is not None
                    and
                    found['numericId'] != id
                ):
                    raise IncorrectFieldSpec(
                        f'Jira custom field "{name}" '
                        f'has different numeric ID'
                        f': {by_name[0]}'
                    )
                return f'customfield_{found["numericId"]}'

        if id is not None:
            by_id = [
                field
                for field in state.customfields
                if field['numericId'] == id
            ]
            if not by_id:
                raise IncorrectFieldSpec(
                    f'Failed to find Jira custom field'
                    f' by numeric ID: {id}'
                )
            assert len(by_id) == 1
            found = by_id[0]
            if (
                name is not None
                and
                found['name'] != name
            ):
                raise IncorrectFieldSpec(
                    f'Jira custom field with ID {id}'
                    f'has different name: {found["name"]}.'
                    f' Expected: {name}'
                )

            return f'customfield_{found["numericId"]}'
        raise ValueError((name, id))

    return wrapped


def import_lines(state):
    http = state.http
    for line in sys.stdin:
        jissue = json.loads(line) 
        result = post_json(
            http,
            '/rest/api/2/issue',
            jissue,
        )
        json.dump(
            result,
            sys.stdout,
        )
        print()


def attach_yissue_json(state, yissue):
    ykey = state.key(yissue)
    jkey = map_ykey(state, ykey)
    file = (
        f'{ykey}.json',
        json.dumps(yissue),
       'application/json',
    )
    return post_file(
        state.http,
        f'/rest/api/2/issue/{jkey}/attachments',
        dict(file=file)
    )


def with_pending_json_uploads(state, yissues):
    for yissue in yissues: 
        ykey = state.key(yissue)
        try:
            jkey = map_ykey(state, ykey)
        except IssueKeyMappingError:
            continue

        found = [
            item
            for item in jissue_attachments(
                state,
                yissue
            )
            if (
                item['filename'] == f'{ykey}.json'
            ) and (
                item['mimeType'] == 'application/json'
            )
        ]
        if any(found):
            continue

        yield yissue 


def create_jlink(state, jlink):
    return post_json(
        state.http,
        '/rest/api/2/issueLink',
        jlink,
    )


def bluk_create_jissues(state, data):
    return post_json(
        state.http,
        '/rest/api/2/issue/bulk',
        data
    )


def yissue_attachments(state, yissue):
    return [
        item
        for item in yissue['attachments']
        if (
            not item['removed']
            and
            not item['draft']
        )
    ]


def attachment_path(state, yissue, item):
    ykey = state.key(yissue)
    project_key = ykey.split('-')[0]
    return Path(
        state.attachments_dir,
        project_key,
        ykey,
        item['id'],
    )


def attachment_url(state, yissue, item):
    base_url = os.environ['YT_URL']
    return f'{base_url}{item["url"]}'


def pending_download_items(state, yissue):
    for item in yissue_attachments(state, yissue):
        path = attachment_path(state, yissue, item)
        exists = path.exists()
        size = (
            None
            if not exists
            else path.stat().st_size
        )
        if exists and size == item['size']:
            continue

        yield dict(
            path=path.as_posix(),
            item=item,
            exists=exists,
            size=size,
        )


def with_pending_downloads(state, yissues):
    for yissue in yissues:
        items = list(
            item['item']
            for item in pending_download_items(
                state,
                yissue
            )
        )

        if not items:
            continue

        yield yissue, items


def prepare_curl_download_args(state, yissue, items):
    def curl_args(items):
        yield 'curl'
        yield '--location'
        # yield '--silent'
        yield '--show-error'

        # XXX
        yield '--insecure'

        yield '--max-time'
        yield '3600'
        yield '--retry'
        yield' 5'
        yield '--retry-delay'
        yield '0'
        yield '--retry-max-time'
        yield '120'

        yield '--create-dirs'
        # yield '--continue-at'
        # yield '-'
        for item in items:
            yield '--output'
            yield attachment_path(
                state,
                yissue,
                item
            ).as_posix()
            yield attachment_url(state, yissue, item)

    return list(curl_args(items))


def download_attached_files(state, yissue, items):
    curl_args = prepare_curl_download_args(
        state,
        yissue,
        items
    )
    with start_action(
        action_type='curl-get',
        args=curl_args,
        cmd=' '.join(curl_args),
        ykey=state.key(yissue),
    ) as action:
        try:
            curl = subprocess.run(
                curl_args,
                check=True,
                capture_output=True,
            )
        except subprocess.CalledProcessError as e:
            action.log(
                message_type='curl-error',
                stderr=str(e.stderr),
                stdout=str(e.stdout),
                returncode=e.returncode, 
            )
            raise


def jissue_attachments(state, yissue):
    ykey = state.key(yissue)
    jkey = state.ykey_to_jkey[ykey]
    jissue = state.jissues[jkey]
    return (
        jissue['fields']['attachment']
    )


def matching_attachments(item, jitems):
    return [
        jitem 
        for jitem in jitems
        if (
            jitem['filename'] == 
            item['name']
        ) and (
            jitem['size'] ==
            item['size']
        )
    ]


def pending_upload_items(state, yissue):
    ykey = state.key(yissue)
    try:
        jkey = map_ykey(state, ykey)
    except IssueKeyMappingError:
        return []

    items = yissue_attachments(state, yissue)
    if not items:
        return []

    jitems = jissue_attachments(state, yissue)

    return [
        item
        for item in items
        if not any(matching_attachments(item, jitems))
    ]


def with_pending_uploads(state, yissues):
    for yissue in yissues:
        items = pending_upload_items(state, yissue)
        if not items:
            continue

        yield yissue, items


def prepare_curl_upload_args(state, yissue, items):
    ykey = state.key(yissue)
    jkey = map_ykey(state, ykey)

    def file_arg(item):
        path = attachment_path(
            state, yissue, item
        )
        def escape(arg):
            return ( 
                arg
                .replace('\\', '\\\\')
                .replace('"', '\\"')
            )
        return (
            f'file=@{path};'
            f'type={item["mimeType"]};'
            f'filename="{escape(item["name"])}"'
        )

    def curl_args(items):
        yield 'curl'
        # yield '--silent'
        yield '--show-error'

        yield '--max-time'
        yield '3600' 
        yield '--retry'
        yield' 5'
        yield '--retry-delay'
        yield '0'
        yield '--retry-max-time'
        yield '120'

        # XXX
        yield '--insecure'

        yield '--header'
        yield 'X-Atlassian-Token: no-check'

        yield '--request'
        yield 'POST'

        yield '--user'
        yield os.environ['JIRA_AUTH']

        for item in items:
            path = attachment_path(
                state,
                yissue,
                item,
            )
            if not path.exists():
                log_message(
                    message_type='missing-file',
                    path=path.as_posix(),
                    ykey=ykey,
                )
                continue
            yield '--form'
            yield file_arg(item)

        yield jurl(
            f'/rest/api/2/issue/{jkey}/attachments'
        )

    return list(curl_args(items))


def upload_attached_files(state, yissue, items):
    curl_args = prepare_curl_upload_args(
        state,
        yissue,
        items,
    )
    ykey = state.key(yissue)
    with start_action(
        action_type='curl-post',
        args=curl_args,
        cmd=' '.join(curl_args),
        ykey=ykey,
    ) as action:
        try:
            curl = subprocess.run(
                curl_args,
                check=True,
                capture_output=True,
            )
        except subprocess.CalledProcessError as e:
            action.log(
                message_type='curl-error',
                stderr=str(e.stderr),
                stdout=str(e.stdout),
                returncode=e.returncode, 
            )
            raise

# Override globals
config_py = os.environ.get('CONFIG_PY', 'config.py')
with open(config_py) as f:
    code = compile(
        f.read(),
        config_py,
        'exec',
    )
    exec(
        code,
        globals(),
        locals()
    )


if __name__ == '__main__':
    main()


    # http = http_session()
    # json.dump(
    #     fetch_project(http, 'TMP'),
    #     sys.stdout,
    # )
    # print()
    # json.dump(
    #     get_json(
    #         http,
    #         f'/rest/api/2/issueLinkType',
    #     )['issueLinkTypes'],
    #     sys.stdout,
    # )
    # print()