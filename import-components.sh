#!/bin/sh
YT_PROJECT_KEY=${1:?}
PROJECT_KEY=${2:?}
FIELD=${3:?}

DEFAULT=$(
	./get-project-custom-field.sh \
		$YT_PROJECT_KEY \
		"$FIELD" \
	| jq -r '.field.fieldDefaults.emptyFieldText | strings'
)

ARGS=$(
	jq --arg PROJECT_KEY "PROJECT_KEY" \
		--null-input \
		'{PROJECT_KEY: $PROJECT_KEY}'
)
[ -n "$DEFAULT" ] && {
	ARGS=$(jq \
		--arg PROJECT_KEY "$PROJECT_KEY" \
	    --arg DEFAULT "$DEFAULT" \
		--null-input \
		'{PROJECT_KEY: $PROJECT_KEY, DEFAULT: $DEFAULT}'
	)
} 

./get-bundle-values.sh $YT_PROJECT_KEY "$FIELD" \
| jq -c --argjson args "$ARGS" '
	.[]
	| select(
		$args.DEFAULT == null
		or
		.name != $args.DEFAULT
	)
	| { 
		name, 
	 	project: $args.PROJECT_KEY
	}' \
| while read line ; do
	echo "$line" | ./jira-post.sh component
done