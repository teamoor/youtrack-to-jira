select(
  .name
  | IN($ARGS.positional[])
) | {
  name: .name,
  inward: .sourceToTarget,
  outward: .targetToSource,    
}