#!/usr/bin/env jq -r -f

[
  .[]
  | select(
      (.parent.issues | length) > 0
    )
  | .customFields[]
  | select(
      .name == "Type"
    )
  | .value.name
]
| unique
| .[]