#!/bin/sh
set -e

YT_PROJECT_KEY=${1:?}
PROJECT_KEY=${2:?}

shift
shift


mkdir -p "input/$YT_PROJECT_KEY"
ISSUES_JSON="input/$YT_PROJECT_KEY/issues.json"


BATCH_SIZE=${1:-${BATCH_SIZE:-${DEFAULT_BATCH_SIZE}}}
CHUNK_SIZE=${2:-${CHUNK_SIZE:-${DEFAULT_CHUNK_SIZE}}}
shift
shift

LAST=${1:-${LAST}}

VERSION_FIELD="${VERSION_FIELD}"
COMPONENT_FIELD="${COMPONENT_FIELD}"
RUN_GROOVY=${RUN_GROOVY:-"true"}
GROOVY_TMPL=${GROOVY_TMPL:-"updateFromAttachedJSON.groovy.tmpl"}
GROOVY_BASENAME=${GROOVY_TMPL%%.groovy.tmpl}


test -r $ISSUES_JSON || {
	echo "fetching YouTrack issues to $ISSUES_JSON"
	time ./get-project-issues.sh $YT_PROJECT_KEY 99999 0 > $ISSUES_JSON 

	echo "fetching YouTrack attachments"
	time python ytconvert.py $PROJECT_KEY \
		--fetch-files < $ISSUES_JSON

	python ytconvert.py $PROJECT_KEY \
		--check-files < $ISSUES_JSON \
		| jq -r -c 'select(.item.size == 0) | .path' \
		| xargs --no-run-if-empty touch
}

[ -n "$LAST" ] && [ $LAST -gt 0 ]  && {
	SLICED_ISSUES_JSON="input/$YT_PROJECT_KEY/last-$LAST-issues.json"
	jq \
		--argjson N $LAST \
		--compact-output \
		'.[-($N):]' \
		$ISSUES_JSON \
	> $SLICED_ISSUES_JSON 
	ISSUES_JSON=$SLICED_ISSUES_JSON
}
TOTAL_ISSUES=$(jq 'length' $ISSUES_JSON)

[ -n "$VERSION_FIELD" ] && {
	./import-versions.sh $YT_PROJECT_KEY $PROJECT_KEY "$VERSION_FIELD"
	echo "imported YouTrack versions: $VERSION_FIELD"
}

[ -n "$COMPONENT_FIELD" ] && {
	./import-components.sh $YT_PROJECT_KEY $PROJECT_KEY "$COMPONENT_FIELD"
	echo "imported YouTrack components: $COMPONENT_FIELD"
}

(
	echo "fetching attached files from YouTrack in background"

	python ytconvert.py $PROJECT_KEY \
		--fetch-files \
		--no-progress \
		< $ISSUES_JSON

	python ytconvert.py $PROJECT_KEY \
		--check-files \
		--no-progress \
		< $ISSUES_JSON \
		| jq -r -c 'select(.item.size == 0) | .path' \
		| xargs --no-run-if-empty touch
) &

COUNT=1
while true ; do

	LABEL="YouTrack-${YT_PROJECT_KEY}-$(date +'%F-%H%M%S')"
	echo
	echo

	echo "starting batch $COUNT, label: $LABEL"
	echo

	STARTED_AT=$(date +%s)

	time python ytconvert.py \
		$PROJECT_KEY \
		--bulk \
		--map \
		--chunk-size $CHUNK_SIZE \
		--limit $BATCH_SIZE \
		--start-at $((($COUNT - 1) * $BATCH_SIZE)) \
		--create \
		--label "$LABEL" \
		< $ISSUES_JSON

	jq 'select(
			.message_type == "response"
			and
			.status == 201
			and (
				.content
				| .errors
				| length > 0
			)
		) | {
			task_uuid,
			content
		}
		' \
		output/$PROJECT_KEY/log/bulk.json \
		> output/$PROJECT_KEY/log/bulk.errors.json

	test -s output/$PROJECT_KEY/log/bulk.errors.json && {
		echo
		echo
		jq -c '
			.task_uuid as $uuid
			| .content.errors[]
			| {
				task_uuid: $uuid,
				failedElementNumber, elementErrors}
			' \
			output/$PROJECT_KEY/log/bulk.errors.json \
		| while read jsonline ; do
			jq  '
				select(
					.task_uuid == $item.task_uuid
					and
					.action_status == "started"
					and
					.action_type == "http-post"
				) | {
					update: .data.issueUpdates[
						$item.failedElementNumber
					],
					errors: $item.elementErrors
				}
			' --argjson item "$jsonline" \
			> output/$PROJECT_KEY/log/bulk.failed-tasks.json
		done
		"Some issues failed. Check output/$PROJECT_KEY/log/bulk.errors.json"
		exit 1
	}
	echo
	echo
	echo "finished batch $COUNT"
	echo
	echo

	python ytconvert.py \
		$PROJECT_KEY \
		--links \
		--create \
		--label "$LABEL" \
		--no-progress \
		< $ISSUES_JSON &

	python ytconvert.py \
		$PROJECT_KEY \
		--post-files \
		--label "$LABEL" \
		--no-progress \
		< $ISSUES_JSON &

	time python ytconvert.py \
		$PROJECT_KEY \
		--attach-json \
		--label "$LABEL" \
		< $ISSUES_JSON 

	[ \
		"$RUN_GROOVY" == "yes" -o \
 		"$RUN_GROOVY" == "true" -o \
 		"$RUN_GROOVY" == "1" \
	] && {
		echo 
		echo "running Groovy script"
		echo
		echo
		python ytconvert.py \
			$PROJECT_KEY \
			--groovy-script \
				"$GROOVY_TMPL" \
			--label "$LABEL" \
			< $ISSUES_JSON

		time curl \
			--show-error \
			--silent \
			--insecure \
			-u "$JIRA_AUTH" \
			-X POST \
			"$JIRA_URL/rest/scriptrunner/latest/user/exec/" \
			-H "X-Atlassian-token: no-check"\
			-H "Content-Type: application/x-www-form-urlencoded; charset=UTF-8" \
			-H "Accept: application/json" \
			--data-urlencode "scriptText@${GROOVY_BASENAME}.${LABEL}.groovy" \
			"$JIRA_URL/rest/scriptrunner/latest/user/exec/" \
			> "output/$PROJECT_KEY/groovy-script.$LABEL.json"
		echo
		echo

		jobs -p | xargs ps -p | tail -n+2

		echo
		echo
	}

	NOW=$(date '+%s')
	echo "took $(($NOW - $STARTED_AT))s"

	COUNT=$(($COUNT + 1))
	TOTAL_IMPORTED=$(
		python ytconvert.py \
			$PROJECT_KEY \
			--map \
			< $ISSUES_JSON \
		| jq 'to_entries | length' 
	)

	echo -e "\timported $TOTAL_IMPORTED/$TOTAL_ISSUES "

	echo
	echo

	[ $TOTAL_IMPORTED -ge $TOTAL_ISSUES ] && {
		break
	}
done

echo "creating missing links"
python ytconvert.py \
	$PROJECT_KEY \
	--links \
	--create \
	< $ISSUES_JSON
