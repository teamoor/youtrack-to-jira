#!/bin/sh
FIELD=${1:?}
GROUP_BY=${2:?}
jq -r --arg field "$FIELD" --arg groupby "$GROUP_BY" '
  [
    .[]
    | {
      ($field): (.customFields[] | select(.name == $field) | .value.name),
      ($groupby): (.customFields[] | select(.name == $groupby) | .value.name)
    }
  ]
  | group_by(.[$groupby])
  | [ .[] | unique_by(.[$field]) ]
  | [ .[] | {(.[0][$groupby]): [.[] | .[$field]]} ]
  | add
'