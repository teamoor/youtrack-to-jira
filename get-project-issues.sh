#!/bin/sh

PROJECT_KEY=${1:?}
TOP=${2:-"50"}
SKIP=${3:-"0"}
EXTRA=${4:-"sort by: created asc"}

YT_FIELDS=${YT_FIELDS:-"${YT_ISSUE_FIELDS:?}"}

FIELDS=$(
    echo "$YT_FIELDS" | ./strip-and-mash-lines.sh 
)

http \
	--body \
	--verify=no \
	GET \
	"$YT_URL/api/issues" \
	\
	"\$skip==$SKIP" \
	"\$top==$TOP" \
	"fields==$FIELDS" \
	"query==project: $PROJECT_KEY $EXTRA" \
	\
	Accept:application/json \
    "Authorization:Bearer $YT_TOKEN" \
	"Content-Type:application/json"