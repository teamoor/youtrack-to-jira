PROJECT_KEY=${1:?}
FIELD=${2:?}
 ./get-project-custom-fields.sh "$PROJECT_KEY" | jq --arg FIELD "$FIELD" '.customFields[] | select(.field.name == $FIELD)'
