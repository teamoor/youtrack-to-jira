#!/bin/sh
jq --compact-output \
   --from-file ./convert-link-types.jq \
   --args "$@"
 