#!/usr/bin/env jq -r -f

[ .[].links[].linkType ] | unique_by(.name) | .[]