#!/usr/bin/env jq -r -f

[
  .[]
  | select(
      (.subtasks.issues | length) > 0
    )
  | .customFields[]
  | select(
      .name == "Type"
    )
  | .value.name
]
| unique
| .[]