#!/usr/bin/env jq -r -f

[
  .[]
  | select(
      (.parent.issues | length) > 1
    )
  | .idReadable
]
| unique
| .[]