#!/bin/sh
set -e
URI=${1:?}
shift

http \
	--body \
	--verify=no \
	--session=".yt-session.json" \
	GET \
	"$YT_URL/api/$URI" \
	"$@" \
	Accept:application/json \
    "Authorization:Bearer $YT_TOKEN" \
	"Content-Type:application/json"