#!/bin/sh
SRC=${1:?}
DST=${2:?}
NAME=${3:-$2}

SCRIPT="com.onresolve.scriptrunner.canned.jira.admin.CopyProject"

jq \
	--null-input \
	--arg SRC "$SRC" \
	--arg DST "$DST" \
	--arg NAME "$NAME" \
	--arg SCRIPT "$SCRIPT" \
	'{
		FIELD_SOURCE_PROJECT: $SRC,
		FIELD_TARGET_PROJECT: $DST,
		FIELD_TARGET_PROJECT_NAME: $NAME,
		("canned-script"): $SCRIPT,
	}' \
| http \
	--verify=no \
	--auth="$JIRA_AUTH" \
	POST \
    "$JIRA_URL/rest/scriptrunner/latest/canned/$SCRIPT" \
    "Accept:application/json"

