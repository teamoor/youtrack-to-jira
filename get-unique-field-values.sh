#!/bin/sh
FIELD=${FIELD:-${1:?}}

BARE=
echo "$@" | grep '\-\-bare' > /dev/null && BARE="yes"

# Standard field (not in .customFields) starts with a lower case letter
case ${FIELD:0:1} in
  ([[:lower:]]) STD="yes" ;;
  (*) STD="" ;;
esac

if [ -n "$STD" ] ; then
  jq -r --arg field "$FIELD" '
    [
      .[] | .[$field]
    ]
    | unique | .[]
  ' 
  exit $?
fi

if [ -n "$BARE" ] ; then
  jq -r --arg field "$FIELD" '
    [
      .[]
      | .customFields[]
      | select(.name == $field)
      | .value
    ]
    | unique | .[]
  ' 
else
  jq -r --arg field "$FIELD" '
    [
      .[]
      | .customFields[]
      | select(.name == $field)
      | .value.name
    ]
    | unique | .[]
  ' 

fi