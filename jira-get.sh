#!/bin/sh
set -e
URI_PATH=${1:?}
shift

http \
	--verify=no \
	--auth="$JIRA_AUTH" \
	--session=".jira-session.json" \
	GET \
    "$JIRA_URL/rest/api/2/$URI_PATH" \
	"$@" \
    "Accept:application/json" 
