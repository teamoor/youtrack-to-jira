#!/bin/sh
USERNAME=${1:?}
http \
	--body \
	--ignore-stdin \
    --auth="$JIRA_AUTH" \
    --verify=no \
    GET "$JIRA_URL/rest/api/2/user/search" \
    "username==$USERNAME" \
    "includeInactive==false" \
| jq  --arg username "$USERNAME" '{key: $username,  value: .}'